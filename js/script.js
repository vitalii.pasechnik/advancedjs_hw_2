"use strict";

import { books } from "./books.js";

const container = document.querySelector('#root');

// В первом варианте функция ищет точные совпадения со всеми тремя ключами. 
// В этом варианте зараннее известно, что будет ошибка, если в объекте не хватает нужных ключей.
// Здесь можно было обойтись конструкцией if...else... т.к. данная функция не упадет, если наткнется на зараннее известное исключение.

const createList1 = (arr, parent = document.body) => {

    const list = document.createElement('ul');
    list.className = "list";
    const regexp = /price|name|author/gi;
    let bookCount = 0;

    arr.forEach((obj, i) => {
        const str = Object.keys(obj).join('');
        const arrOfMatches = str.match(regexp);

        try {
            if (arrOfMatches.length >= 3) {
                const book = document.createElement('li');
                const bookDescr = document.createElement('ul');
                book.className = "book";
                book.innerHTML = `book-${++bookCount}:`;

                Object.entries(obj).forEach(el => {
                    const descrItem = document.createElement('li');
                    descrItem.innerHTML = `${el.join(': ')};`;
                    bookDescr.append(descrItem);
                })
                book.append(bookDescr);
                list.append(book);

            } else throw new Error;

        } catch (err) {
            if (arrOfMatches.length !== 3) {
                const missingValues = ['author', 'name', 'price'].filter(value => arrOfMatches.every(el => value !== el));
                console.warn(`${err.name}: book[${i}] is missing value: ${missingValues.join(', ')};`);
            } else {
                console.error(err);
            }
        }
    })
    parent.append(list);
};

createList1(books, container);


// Во втором варианте я предполагаю, что все объекты имеют 3 нужных ключа и пытаюсь отрисовать 3 поля, зараннее не прописывая возможные исключения.
// В таком варианте приложение упадет, когда наткнется на ошибку. Но с помощью конструкции try...catch... мы можем этого избежать.
// В данном случае отлавливаются все ошибки (известные и не известные), но приложение при этом не падает.

const createList2 = (arr, parent = document.body) => {

    const list = document.createElement('ul');
    list.className = "list";
    let bookCount = 0;

    arr.forEach((obj, i) => {
        const mainProp = ['author', 'name', 'price'];
        const book = document.createElement('li');
        const bookDescr = document.createElement('ul');
        book.className = "book";

        try {
            mainProp.forEach((_, i) => {
                const descrItem = document.createElement('li');
                descrItem.innerHTML = `${Object.entries(obj)[i].join(': ')};`;
                bookDescr.append(descrItem);
            })
            book.innerHTML = `book-${++bookCount}:`;
            book.append(bookDescr);
            list.append(book);

        } catch (err) {
            if (Object.keys(obj).length < 3) {
                const missingValues = mainProp.filter(value => Object.keys(obj).every(el => value !== el));
                console.warn(`${err.name}: book[${i}] is missing property: ${missingValues.join(', ')};`);
            } else {
                console.error(err);
            }
        }
    })
    parent.append(list);
};

createList2(books, container);


// добавил 3-й способ

const createList3 = (arr, parent = document.body) => {

    const list = document.createElement('ul');
    list.className = "list";
    let bookCount = 0;
    const prop = ['author', 'name', 'price'];

    arr.forEach((obj, i) => {
        const book = document.createElement('li');
        const bookDescr = document.createElement('ul');
        book.className = "book";
        const { author, name, price } = obj;

        try {
            const descrItem = !!author && !!name && !!price && document.createElement('li');
            Object.entries(obj).forEach(book => {
                descrItem.insertAdjacentHTML('beforeend', `<li>${book[0]}: ${book[1]}</li>`)
            })
            bookDescr.append(descrItem);
            book.innerHTML = `book-${++bookCount}:`;
            book.append(bookDescr);
            list.append(book);
        } catch (err) {
            prop.forEach(str => !obj[str] && console.warn(`${err.name}: book[${i}] is missing property: ${str}`));
        }
    })
    parent.append(list);
};

createList3(books, container);